<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RedeemedCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redeemed_codes', function(Blueprint $table){
            $table->increments('id')->unsigned()->unique();
            $table->integer('user_id')->index()->unsigned();
            $table->integer('code_id')->index()->unsigned()->unique();
            $table->string('redeemed_id')->nullable()->unique();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('code_id')->references('id')->on('code_catalog');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redeemed_code');
    }
}
