<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('uid');
            /*  $table->unsignedInteger('deck_id'); */
            $table->string('nombre_deck');
            $table->double('precio_unitario');
            $table->integer('qty');
            $table->string('purchase_from')->nullable();
            $table->unsignedInteger('user_id');
            $table->string('user_paypal_email')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras');
    }
}
