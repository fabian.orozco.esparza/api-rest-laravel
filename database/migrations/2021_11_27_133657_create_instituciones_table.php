<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitucionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instituciones', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string("nombre_evento")->unique();
            $table->string("institucion");
            $table->longText('descripcion')->nullable();
            $table->string('image');
            $table->double('precio');
            $table->integer('qty');
            $table->string("uid")->unique();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tec_monterrey');
    }
}
