<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCatalogCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
        if(!Schema::hasColumn('code_catalog', 'deck_id')) {
            Schema::table('code_catalog', function (Blueprint $table) {
                $table->integer('deck_id')->index()->unsigned();
                $table->foreign('deck_id')->references('id')->on('decks');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('code_catalog', function (Blueprint $table) {
                $table->dropColumn('deck_id');
        });
    }
}
