<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class codeCatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('code_catalog')->insert([
            'code' => '4XOL-4Y2U-B2W8-8LYK',
            'redeemed' => 0,
            'deck_id' => 4
        ]);

        DB::table('code_catalog')->insert([
            'code' => 'WE2F-WA3D-6FI9-77JI',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        
        DB::table('code_catalog')->insert([
            'code' => 'QBWC-ZI74-AG30-MBI6',
            'redeemed' => 0,
            'deck_id' => 4
        ]);

        DB::table('code_catalog')->insert([
            'code' => 'N3J1-85ID-7ENX-2ZHH',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '66TD-D5RF-QVVV-075P',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'XJQG-STMZ-N609-M6TE',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'WKTF-0UWR-KJGZ-AKTC',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'QO63-ATP0-AS2Q-8BLP',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'NA4A-1S8P-D7W2-MVP1',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'W1ND-M931-U23V-2DMP',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '94LI-SCK8-A70E-22FN',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'VW8F-HTDG-YBOV-MGGY',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'UWUY-5JOY-94BI-X9ZA',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '2C6T-D3VC-HCYT-ZQ56',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'SZ4D-D5N6-G0F9-B1I5',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'SEVQ-3HL8-UXNI-KD2G',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'IAFG-1NAC-3JYL-X3SG',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'P1Q2-BKA3-5BXD-CHHS',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '2SCS-BVU4-STMI-5AVV',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'V8AX-9G7U-7V45-CB7D',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'QWZS-45JH-4CKX-I9BB',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'DSNR-QFNL-M553-0NE7',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '2O7U-KT8N-JN5M-8DOQ',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'AYUZ-N473-FH1L-LBT3',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '10W6-ZMG1-Z9RJ-SD7X',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '4ELT-RBNZ-K34G-88NH',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'AW9D-QQQR-VFF0-77XS',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'HB0A-B1SP-5FD2-ZULK',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '9W5K-C489-OCYH-N432',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'L1HJ-TW6G-DUER-18YH',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'HHAF-31V4-AXDY-FH3A',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'D9IB-NFYB-IZES-J262',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '69X6-ADK7-ZI3B-7403',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'S0NZ-E57Z-IJ8N-LGAK',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'R9WG-0TED-GCP2-GOV0',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'OEBH-O0EV-JAIV-BKA4',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'VVMB-X8OS-LW37-YFMK',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '90ZM-ISUU-P15V-UXS5',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'J8QT-WKO0-V898-Y6HB',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'AMRH-RCD6-O4RJ-LW91',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '3QVQ-MP2E-M1WL-Y2O9',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'Q3DM-NCX5-PG84-YGBM',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'YDLB-7QTX-MJAE-UHSA',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'KERP-VXO2-J422-NEX8',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'U3Y8-AWAI-EFDP-SM9F',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'CO9R-Y5GX-M6QV-NRZR',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '5KU4-JE7F-M54Q-RPGV',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'NHNV-74EG-7DS5-11UF',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '4V59-OJYN-UPYL-UXNR',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'MA8S-4NNE-IELZ-YT7B',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'MZQF-9VA6-U7CS-LSCR',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'LQK1-PZ60-IJRD-ZOR0',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'B8OX-1XOC-3K2D-6AEI',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '3BY7-HNPL-23OD-RM0K',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '1FCD-8RMI-1PLC-DAYK',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'K1ZS-4OEU-9CV8-C70I',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'KPY7-OQ0X-VSSL-89YW',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'KEHE-NY43-3CDO-4KJI',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'WKKC-UW71-89XA-ICZQ',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '13NO-8Q50-AWCN-VK14',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'SN35-VBIO-DVDV-O7PM',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'SXTI-PPBO-2OJK-PZJ0',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'ZQDO-4DHW-A1MU-Z42L',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'KDVM-FX19-HFDZ-TDUE',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'OTE9-35X8-QCNK-VTH7',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '6UI0-V08C-KQOC-FZOT',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'PF8E-LQZ9-CI64-Y230',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'WU00-JPHP-O2JI-O2J0',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'PXTV-CIUF-6JBJ-GEBO',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'L316-9LTU-7I1Q-RZP3',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '9NN9-SFAV-VHZ6-T6FP',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'Q665-RG3B-K1UW-XC8J',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'DXGI-JHG7-N1QI-4NAC',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'K602-1K7J-8QIS-YJQH',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'P8UC-HNU0-Z334-EVP9',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'EAG9-2ZQ4-16YJ-979Y',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '5W3H-U7H8-S852-81ZF',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '1NWM-IA0V-LJBQ-6MU7',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'IBHS-0FN2-NO8Q-TQYO',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'W250-9KLD-4EBG-IOE2',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'RVU2-TJR6-YFGG-UIZ1',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'CGLR-WWW3-7J94-H8HK',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'HRIC-ZO0J-0FEW-RLQ3',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '99UU-RADB-A74A-9EII',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'EF59-5IHZ-3KQV-VRWH',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'LZNH-IPWI-DXXM-B29I',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'BEM2-V8CD-O9P7-U2DC',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'O5CO-38BV-E2XL-IKRB',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'M5WD-8B15-DNRL-FSG1',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '0EH4-JIDQ-ZVUU-8EQQ',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '810Z-2C1A-KI6U-H85P',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '0KLC-D695-NUUN-DIHX',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '5Y2O-RDPS-LIM2-NUJ3',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'D0UC-4DOG-BN95-ZDJB',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'KKNC-D0RR-U7V9-PY9B',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '3U1N-JQ2M-SJAB-4ZNH',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'RUR1-7ERC-G0PH-9WT5',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'LIY1-M2SN-2VRI-IQ1H',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => '7HMK-WMMH-3R4F-JIFF',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
        DB::table('code_catalog')->insert([
            'code' => 'Z1ZJ-T1J2-AIV6-YNQ9',
            'redeemed' => 0,
            'deck_id' => 4
        ]);DB::table('code_catalog')->insert([
            'code' => 'ZXZD-S8DI-IT5X-330K',
            'redeemed' => 0,
            'deck_id' => 4
        ]);
/* ME quedé en el cógido 102 del archivo de excel */
        
    }
}
