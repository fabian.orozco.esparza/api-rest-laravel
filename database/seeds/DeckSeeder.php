<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeckSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('decks')->insert([
            'nombre_deck' => 'deck 1',
            'artista' => 'Chuy Gallo',
            'descripcion' => 'deck con sonidos de colección',
            'image' => '100.png',
            'precio' => '150',
            'qty'   => '3',
            'uid'   => '12345',
        ]);

        DB::table('decks')->insert([
            'nombre_deck' => 'deck 2',
            'artista' => 'Chuy Gallo',
            'descripcion' => 'deck 2 sonidos de colección',
            'image' => '200.png',
            'precio' => '150',
            'qty'   => '3',
            'uid'   => '12346',
        ]);
    }
}
