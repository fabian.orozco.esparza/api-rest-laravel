<?php
namespace App\Helpers;

use App\User;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;

class JwtAuth{

    public $key;

    public function __construct()
    {   
        $this->key = 'esto_es_una_clave_secreta-99887766';
        $this->verificationToken = 'clave_secreta_para_verificacion_del_usuario-1m4r41nd3ck5';
    }

    public function signup($email, $password, $getToken = null)
    {
        //Buscar si existe el usuario con sus credenciales
        $user = User::where([
            'email' => $email,
            
        ])->first();

        //Comprobar si son correctas (objeto)
        $signup = false;
        if(is_object($user) && password_verify($password, $user->password))
        {
            $signup = true;
        }
        
        //Generar el token con los datos del usuario identificado
        if($signup)
        {
            $token = array(
                'sub'     => $user->id,
                'email'   => $user->email,
                'name'    => $user->name,
                'surname' => $user->surname,
                'description' => $user->description,
                'image'       => $user->image,
                'verified_at' => $user->email_verified_at,
                'iat'     => time(),
                'exp'     => time() + (7 * 24 * 60 *60)

            );

            $jwt = JWT::encode($token, $this->key, 'HS256');
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
            
            //Devolver los datos decodificados o el token, en función de un parámetro
            if(is_null($getToken))
            {
                $data = $jwt;
            }
            else
            {
                $data = $decoded;

            }

        }else{
            $data = array(
                'status' => 'error',
                'message' => 'Login incorrecto.'
            );
        }


        return $data;

    }

    public function createVerificationToken($email, $name, $surname) {
        $userVerified = false;

        if(isset($email) && isset($name) && isset($surname)) {
            $verificationToken = array(
                'email'   => $email,
                'name'    => $name,
                'surname' => $surname,
                'iat'     => time(),
                'exp'     => time() + (7 * 24 * 60 *60)
            );

            $jwt = JWT::encode($verificationToken, $this->verificationToken, 'HS256');
            $decoded = JWT::decode($jwt, $this->verificationToken, ['HS256']);

            return $data = array (
                'status' => "success",
                'token' => $jwt
                
            );
        }
        else {
            $data = array (
                'status'  => "error",
                'message' => "falta algún parámetro para la creación del token"
            );
        }

        
        

        
    }

    public function checkToken($jwt, $getIdentity = false)
    {
        $auth = false;

        try{
            $jwt = str_replace('"', '', $jwt);
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);

        }
        catch(\UnexpectedValueException $e){
            $auth = false;

        }
        catch(\DomainException $e){
            $auth = false;
        }

        if(!empty($decoded) && is_object($decoded) && isset($decoded->sub))
        {
            $auth = true;
        }
        else{
            $auth = false;
        }

        if($getIdentity){
            return $decoded;
        }

        return $auth;
    }
}