<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $table = 'compras';


    public function deck()
    {
        //Relación de una a uno con tabla Decks
        return $this->belongsTo('App\Deck');
    }

    public function user()
    {
        //Relación de una a uno con tabla users
        return $this->belongsTo('App\User');
    }
}
