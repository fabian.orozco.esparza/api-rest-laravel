<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deck extends Model
{
    use SoftDeletes;

    protected $table = 'decks';
    protected $dates = ['deleted_at'];
    //Relación de 1:n con tabla compras
    public function purchase() {

        return $this->hasMany('App\Compra');
    }

     //Relación de 1:n con tarjetas
    public function card() {

        return $this->hasMany('App\Tarjeta');
    }

    //Relación de 1:n
    public function codeCatalog(){
        return $this->hasOne('App\CodeCatalog');
    }
}
