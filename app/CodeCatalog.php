<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeCatalog extends Model
{
    protected $table = "code_catalog";

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
    ];

    //Relación de 1:n
    public function redeemedCodes(){
        return $this->hasOne('App\redeemCode');
    }

    //Relación de 1:n Inversa (Muchos a 1)
    public function deck(){
        return $this->belongsTo('App\Deck');
    }
}
