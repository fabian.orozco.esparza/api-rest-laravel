<?php

namespace App\Http\Controllers;

use App\Deck;
use App\Institucion;
use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Expr\Cast\Array_;

class cardsController extends Controller
{
    public function __construct()
    { 
        $this->middleware('api.auth', [
            'except' => [
                "getCardImage",
                "getAllDecksNoLogin",
                "getInstitutionalEvents"
            ] 
        ]);

    }

    //Deprecated
    //this method was used when login was required to see home page.
    //Date: 08-11-2021
    public function getAllDecks(Request $request) {

        //Comprobar si el usuario está identificado
        $token      = $request->header('Authorization');
        $jwtAuth    = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);
        $deckArray  = array(); 

        if($checkToken) {
            //Obtener todas las imagenes de los decks
            $decksImgs = Storage::disk('decks')->allFiles();

            foreach($decksImgs as $key => $decksImg){
                //Buscar la información del deck en BD
                $deckData = Deck::where('image', $decksImg)->first();
                array_push($deckArray, $deckData);
            }
            sort($deckArray);
            $data = [
                "response" => $deckArray,
                "code" => 200,
            ];
    
            return response()->json($data['response'], $data['code']);
        }
            return response()->json("Usuario no autorizado", 401);

    }

    public function getAllDecksNoLogin() {
        $deckArray  = array(); 

        //Obtener todas las imagenes de los decks
        $decksImgs = Storage::disk('decks')->allFiles();

        foreach($decksImgs as $key => $decksImg){
            //Buscar la información del deck en BD
            $deckData = Deck::where('image', $decksImg)->first();
            array_push($deckArray, $deckData);
        }
        sort($deckArray);
        $data = [
            "response" => $deckArray,
            "code" => 200,
        ];

        return response()->json($data['response'], $data['code']);
    
    }

    public function getInstitutionalEvents() {
        $deckArray  = array(); 

        //Obtener todas las imagenes de los eventos institucionales
        $decksImgs = Storage::disk('institucional')->allFiles();


        foreach($decksImgs as $key => $decksImg){
            //Buscar la información del deck en BD
            $deckData = Institucion::where('image', $decksImg)->first();
            array_push($deckArray, $deckData);
        }
        sort($deckArray);
        $data = [
            "response" => $deckArray,
            "code" => 200,
        ];

        return response()->json($data['response'], $data['code']);
    
    }

    public function getCardImage($fileName) {
        
            $isset = Storage::disk('decks')->exists($fileName);
            $institucional = Storage::disk('institucional')->exists($fileName);
            if($isset)
            {
                $file = Storage::disk('decks')->get($fileName);
    
                return new Response($file, 200);
            }
            else if($institucional) {
                $file = Storage::disk('institucional')->get($fileName);
    
                return new Response($file, 200);
            }
            else {
                $data = array(
                    'code'    => 404,
                    'status'  => 'error',
                    'message' => 'La imagen no existe.'
                );
            }
            return response()->json($data, $data['code']);
    }
}
