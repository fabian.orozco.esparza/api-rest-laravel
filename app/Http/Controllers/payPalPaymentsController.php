<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Compra;
use phpDocumentor\Reflection\Types\Integer;

class payPalPaymentsController extends Controller
{
    //ask for token
    public function __construct() {
        $this->middleware('api.auth');
    }
   
   
    public function storePaypalInfo(Request $request) {
        //Recoger datos del request
        $json        = $request->input('json', null);
        $params      = json_decode($json);
        $paramsArray = json_decode($json, true);
    
        if(!empty($paramsArray)) {
             //Conseguir usuario identificado
             $jwtAuth = new \JwtAuth();
             $token = $request->header('Authorization', null);
             $user = $jwtAuth->checkToken($token, true);

             if(!empty($user)) {
                //Introducir datos del comprador en la bd
                $purchase_units = $params->purchase_units[0]->items;
                
                foreach ($purchase_units as $key => $item) {
                    $compra = new Compra();
                    $compra->uid            = random_int(4,9);
                    $compra->nombre_deck    = $item->name;
                    $compra->precio_unitario= $item->unit_amount->value;
                    $compra->user_id        = $user->sub;
                    $compra->qty            = $item->quantity;
                    $compra->purchase_from  = "paypal";
                    $compra->user_paypal_email  = $params->payer->email_address;
                    
                    if($compra->save()) {
                        $code = 201;
                        $message = "compra exitosa";
                    }
                    else {
                        $code = 500;
                        $message = "Algo ha salido mal contacta con el administrador del sistema";
                    }
                }

                $data = [
                    'data'     => $compra,
                    'code'     => $code,
                    'message'  => $message
                ];
             }

             

        }

        return response()->json($data, $data['code']);
    }
}
