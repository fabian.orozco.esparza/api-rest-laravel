<?php

namespace App\Http\Controllers;

use App\User;
use App\Deck;
use App\Compra;
use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;


class MisComprasController extends Controller
{

    //ask for token
    public function __construct() {
        $this->middleware('api.auth')->except(['downloadDeck']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Otener usuario autentificado
        $jwtAuth  = new JwtAuth();
        $token    = $request->header('Authorization', null);
        $identifiedUser     = $jwtAuth->checkToken($token, true); 
        $deckNames  = [];
        //obtener la relación del user con las compras 
        $user = User::where('id', $identifiedUser->sub)
                        ->where('email', $identifiedUser->email)->first();
       
        //Buscar los decks por nombre en relación a las compras
        foreach ($user->purchase as $key => $compra) {
            array_push($deckNames, $compra->nombre_deck);
        }

        $decks = Deck::whereIn('nombre_deck', $deckNames)->get();
    
        //retornar un objeto con los decks comprados
        return response()->json($decks, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json("hola desde create", 200);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadDeck($deckName) {
        ini_set('memory_limit','300M');
        ini_set('output_buffering','0');

        
        //Verificar usuario identificado 
        /* $jwtAuth  = new JwtAuth();
        $token    = $request->header('Authorization', null);
        $identifiedUser     = $jwtAuth->checkToken($token, true); */
        //Recoger parámetros
       /*  $json = $request->input('json');
        $params = json_decode($json, null);
        $paramsArray = json_decode($json, true);
        $fileName = $params->purchasedDeck; */
        //buscarlo en el storage
        //comprobar si existe el fichero
       /*  $isset = Storage::disk('zip')->exists($fileName); */

           /*  $fs = Storage::getDriver();
            $stream = $fs->readStream("http://api-rest-laravel.com.test/zip/imarain_deck_1er_edicion.zip");

            return response()->stream(
                function() use($stream) {
                    while(ob_get_level() > 0) ob_end_flush();
                    fpassthru($stream);
                },
                200,
                array('Content-Type: application/zip')); */
            //Conseguir el zip del deck
            //$file = Storage::disk('zip')->get($fileName);
            
            //Devolver la imagen
            
           // return response()->download(asset('zip/imarain_deck_1er_edicion.zip'), 'imarain_deck_1er_edicion.zip', array('Content-Type: application/zip'));
           $deckName = str_replace('.png', '.zip', $deckName);
           $downloadPath = (public_path() . "/zip" . "/".$deckName);
           return ( response()->download($downloadPath));
/*             return response()->download("http://api-rest-laravel.com.test/zip/imarain_deck_1er_edicion.zip"); */
        


       /*  if($isset) {
            //Conseguir el zip del deck
            $file = Storage::disk('zip')->get($fileName);

            $callback = function() use($file) {
              $handle =  fopen(asset('zip/imarain_deck_1er_edicion.zip'),'r');
              fclose($handle);
            };
           
            $headers = [
                'Content-Type' => ' application/zip',
            ];
            //Devolver la imagen
            return response()->streamDownload($callback,'imarain_deck_1er_edicion.zip', $headers);
        } */
        
    }
}
