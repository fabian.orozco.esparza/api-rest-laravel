<?php

namespace App\Http\Controllers;

Use App\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('api.auth', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return response()->json([
            'code' => 200,
            'status' => "success",
            'categories' => $categories,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Recoger los datos por post
        $json = $request->input('json');
        $params_array = json_decode($json, true);

        if(!empty($params_array))
        {
            //Validar los datos
            $validate = \Validator::make($params_array, [
                'name' => 'required'
    
            ]);
    
            //Guardar la categoría
                if($validate->fails())
                {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'No se ha guardado la categoría.'
                    ];
                }
                else
                {
                    $category = new Category();
                    $category->name = $params_array['name'];
                    $category->save();
    
                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'category' => $category
                    ];
                }
    
                
            }
            else
            {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'No has enviado ninguna categoría.'
                ];
            }
            
            //Devolver el resultado
            return response()->json($data, $data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        if(is_object($category)){
            $data = [
                'code' => 200,
                'status' => "success",
                'category' => $category,
            ];

        }
        else
        {
            $data = [
                'code' => 404,
                'status' => "error",
                'message' => "La categoría no existe.",
            ];
        }

        return response()->json($data, $data['code']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Recoger los datos por post
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);

        if(!empty($params_array))
        {
            //Validar los datos
            $validate = \Validator::make($params_array, [
                'name' => 'required'

            ]);
    
            //Quitar lo que no quiero actualizar
            unset($params_array['id']);
            unset($params_array['created_at']);

            //Actualizar el registro
            $category = Category::where('id', $id)->update($params_array);

            $data = [
                'code' => 200,
                'status' => 'success',
                'category' => $params_array
            ];
            
        }
        else
        {
            $data = [
                'code' => 404,
                'status' => "error",
                'message' => "La categoría no existe.",
            ];
        }
        
        //Devolver respuesta
        return response()->json($data, $data['code']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
