<?php

namespace App\Http\Controllers;

use App\User;
use App\Helpers\JwtAuth;
use App\Mail\PasswordRecover;
use App\Mail\userEmailVerification;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Mail\UserRegisterAlert;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function __construct()
    { 
        $this->middleware('api.auth', 
            [
                'except' => [
                'login', 
                'register', 
                'getImage', 
                'verifyUser', 
                'recoverPassword',
                'updatePassword'
                ]
            ]);

    }

    public function register(Request $request){

        //Recoger datos de usuario por POST
        $json           = $request->input('json', null);
        $params         = json_decode($json); //objeto
        $params_array   = json_decode($json, true); //array

        if(!empty($params) && !empty($params_array))
        {
            //Limpiar datos
            $params_array = array_map('trim', $params_array);
            
            //Validar los datos
            $validate = Validator::make($params_array, [
                'email'     =>  'required|email|unique:users',
                'password'  =>  'required'
    
            ]);
    
            if($validate->fails()){
                //La validación ha fallado
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El usuario no se ha creado',
                    'errors'  => $validate->errors()
                );
    
            }else{
                //Validación correcta
                //Cifrar la contraseña
                  $pwd = password_hash($params->password, PASSWORD_BCRYPT, ['cost' => 4]);

                  //crear un token de verificación
                  $verificationToken = $this->createToken($params_array['email'], $params_array['name'], $params_array['surname']);
                 /*  $jwtAuth    = new JwtAuth();
                  $verificationToken = $jwtAuth->createVerificationToken($params_array['email'], $params_array['name'], $params_array['surname']); */

                  if($verificationToken['status'] == 'success') {
                      //Comprobar si usuario existe (usuario diplicado)
                      
                      //Crear el usuario
                        $user = new User();
                        $user->name     = $params_array['name'];
                        $user->surname  = $params_array['surname'];
                        $user->email    = $params_array['email'];
                        $user->password = $pwd;
                        $user->verification_token = $verificationToken['token'];
                        
                        //Guardar datos en bd
                        $user->save();
                        
                        Mail::to($user->email)->send(new userEmailVerification($verificationToken['token']));
                        //Enviar correo informando a imarain del nuevo registro
                       // Mail::to("imarain.contact@gmail.com")->send(new UserRegisterAlert($user));
    
                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'El usuario se ha creado correctamente',
                        'user'  => $user
                        
                    );

                  }
                  
            }

        } else{
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
                

            );
        }
        
    
       

        return response()->json($data, $data['code']);
    }

    public function createToken($email, $name, $surname) {
        //crear un token de verificación
        $jwtAuth    = new JwtAuth();

        return $jwtAuth->createVerificationToken($email, $name, $surname);
    }

    public function recoverPassword(Request $request) {
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);
        //ubicar usuario por email
        $user = User::where('email', $params_array)->first();

        if(is_object($user)) {
            //crear token de recuperación
            $token = $this->createToken($user->email, $user->name, $user->surname);
            //Guardar token de recuperación en la bd
            if($token['status'] == "success") {
                $user->verification_token = $token['token'];
                $user->save();

                //Enviar correo al usuario
                Mail::to($user->email)->send(new PasswordRecover($token['token']));
                 
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Se ha enviado un correo de recuperación de contraseña al correo electrónico proporcionado, revisa tu inbox. (Pueden tardar unos minutos en que el correo llegue a tu bandeja de entrada, por favor se paciente o revisa tu bandeja de correo no deseado o spam)',
                );
            }
            else {
                $data = array(
                    'status' => 'Server Error',
                    'code' => 500,
                    'message' => 'No se pudo enviar el correo electrónico, intenta mas tarde.',
                );
            }
        }
        else {
            $data = array(
                'status' => 'Bad request',
                'code' => 400,
                'message' => 'Ocurrió un error, el usuario no fue localizado o no existe.',
            );
        }


        return response()->json($data);

    }

    public function verifyUser($token) {
        $data = array();
        $user = User::where('verification_token', $token)->first();
        
        if(is_object($user)) {
            if(is_null($user->email_verified_at) && !is_null($user->verification_token)) {
                $user->email_verified_at = date("Y-m-d H:i:s"); 
                $user->save();
                unset($user->id);

                $data = [
                    "status"    => 'success',
                    "code"      => 200,
                    "response" => $user,
                ];

                
            }
            else {
                $data = [
                    "status"    => 'verified',
                    "code"      => 100,
                    "message" => "El usuario ya ha sido verificado, proceda a iniciar sesión",
                ];
            }
        }
        else {
            $data = [
                "status"    => 'error',
                "code"      => 404,
                "message"   => "Hubo un error al tratar de verificar al usuario, el token es incorrecto o el usuario no existe"
            ];
        }

        return redirect('https://imarain.com/?status='.$data['status'] .'&code='.$data['code']);
    }

    public function login(Request $request){
        $jwtAuth = new JwtAuth();

        //Recibir datos por POST
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);

        //Validar esos datos
        $validate = Validator::make($params_array, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validate->fails()){
            //La validación ha fallado
            $signup = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'El usuario no se ha podido identificar',
                'errors'  => $validate->errors()
            );

        }
        else{
            //Verificar que el user esté verificado 
            //Devolver token o datos
            $signup = $jwtAuth->signup($params->email, $params->password);

            if(!empty($params->getToken))
            {
                $signup = $jwtAuth->signup($params->email, $params->password, true);
                if(!is_null($signup->verified_at)) {
                    return response()->json($signup, 200);
                }
                else {
                    //La validación ha fallado
                    $signup = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'El usuario no se ha verificado',
                        'errors'  => 'Error al iniciar sesión, por favor verifique su cuenta a traves de su correo electrónico'
                    );
                }

            }
        }
    
        return response()->json($signup, 200);
    }

    public function update(Request $request)
    {
        //Comprobar si el usuario está identificado
        $token      = $request->header('Authorization');
        $jwtAuth    = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);

        //Recoger los datos por post
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);

        if($checkToken && !empty($params_array))
        {
           
            //Sacar usuario identificado
            $user = $jwtAuth->checkToken($token, true);

            //Validar los datos
            $validate = Validator::make($params_array, [
                'name'      =>  'required|alpha',
                'surname'   =>  'required|alpha',
                'email'     =>  'required|email|unique:users'.$user->sub
                
            ]);

            //Quitar los campos que no quiero actualizar
            unset($params_array['id']);
            unset($params_array['role']);
            unset($params_array['password']);
            unset($params_array['created_at']);
            unset($params_array['remember_token']);

            //Actualizar usuario en bd
            $user_update = User::where('id', $user->sub)->update($params_array);

            //Devolver array con el resultado
            $data = array(
                'code' => 200,
                'status' => 'success',
                'user' =>  $user,
                'changes' => $params_array,
            );
            
        }else
        {
            $data = array(
                'code' => 401,
                'status' => 'error',
                'message' => "El usuario no está identificado"
            );
            
        }

            return response()->json($data, $data['code']);
        
    }

    public function updatePassword(Request $request)
    {
        //Recoger los datos por post
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);

        if(!empty($params_array))
        {
            $token = $params_array['token'];
            //Limpiar datos de espacios en blanco al principio y al final
            $params_array = array_map('trim', $params_array);

           //verificar que passwords coincidan
           if($params_array['password'] == $params_array['reEnterPass'] && $params_array ['password'] !== "") {
               //Cifrar password
                $params_array['password'] = password_hash($params_array['password'], PASSWORD_BCRYPT, ['cost' => 4]);

                //Quitar los campos que no quiero actualizar
                unset($params_array['reEnterPass']);
                unset($params_array['token']);
                $params_array['verification_token'] = "";
               //Actualizar usuario en bd
               $user_update = User::where('verification_token', $token)->update($params_array);

                if($user_update) {
                    //Devolver array con el resultado
                    $data = array(
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'La contraseña fue actualizada con éxito',               
                    );
                }
                else {
                    $data = array(
                        'code' => 403,
                        'status' => 'Forbidden',
                        'message' => 'La contraseña no fue actualizada debido a un problema de seguridad y no se puede proporcionar el servicio en este momento',                     
                    );
                }
           }
           else {
            $data = array(
                'code' => 400,
                'status' => 'Bad request',
                'message' => 'Las contraseñas no coinciden',       
            );
           }
            
        }
        
        return response()->json($data);   
    }

    public function upload(Request $request)
    {

        //Recoger datos de la petición
        $image = $request->file('file0');

        //Validación de la imagen
        $validate = Validator::make($request->all(), [
            'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        //Guardar imagen
        if(!$image || $validate->fails())
        {   
            $data = array(
                'code'    => 400,
                'status'  => 'error',
                'message' => 'Error al subir imagen'
            );
        }
        else
        {
            $image_name = time().$image->getClientOriginalName();
            Storage::disk('users')->put($image_name, File::get($image));

            $data = array(
                'code' => 200,
                'status' => 'success',
                'image' => $image_name,
            );
            
        }


        return response()->json($data, $data['code']);
    }


    public function getImage($fileName)
    {
        $isset = Storage::disk('users')->exists($fileName);
        if($isset)
        {
            $file = Storage::disk('users')->get($fileName);

            return new Response($file, 200);
        }
        else {
            $data = array(
                'code'    => 404,
                'status'  => 'error',
                'message' => 'La imagen no existe.'
            );
        }
        return response()->json($data, $data['code']);
    }

    public function detail($id)
    {
        $user = User::find($id);

        if(is_object($user))
        {
            $data = array(
                'code' => 200,
                'status' => 'success',
                'data' => $user
            );
        }
        else
        {
            $data = array(
                'code'    => 404,
                'status'  => 'error',
                'message' => 'El usuario no existe.'
            );
        }

        return response()->json($data, $data['code']);
    }

}
