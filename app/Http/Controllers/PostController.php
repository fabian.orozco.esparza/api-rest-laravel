<?php

namespace App\Http\Controllers;

use App\Post;
use App\redeemCode;
use App\CodeCatalog;
use App\Compra;
use App\Helpers\JwtAuth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('api.auth', ['except' => [
            'index', 
            'show', 
            'getImage',
            'getPostsByCategory',
            'getPostsByUser',
            'getCodes',
            'storePaypalInfo'
        ]]);
    }

    public function getCodes() {
      $codigos =  CodeCatalog::all();
        $data = [
            "code" => 200,
            "response" => $codigos
        ];

      return response()->json($data['response'], $data['code']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        $data = [
            "status" => "success",
            "posts" => $posts,
            "code" => 200,
            
        ];

        return response()->json($data, $data["code"]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function redeemCode(Request $request) 
    {

        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);
        
        if(!empty($params_array)) {
            //Conseguir usuario identificado
            $jwtAuth = new JwtAuth();
            $token = $request->header('Authorization', null);
            $user = $jwtAuth->checkToken($token, true);

            // Validar info del request
            $validate = Validator::make($params_array, [
                'codigo' => 'required'
            ]);

            if($validate->fails()) {
                $data = [
                    'code' => 404,
                    'status' => 'Not found',
                    'message' => 'No se ha encontrado un código válido'
                ];
            }
            else {
                // ir a buscar a la bd el código y buscar que exista
                $code = CodeCatalog::where('code', $params_array['codigo'])->first();
                if(is_object($code) && !is_null($code)) {
                    //verify if code is already redeemed
                    if($code->redeemed) {
                        $data = [
                            'code' => 400,
                            'status' => 'Bad request',
                            'message' => 'El código ya ha sido canjeado'
                        ];
                    }
                    else {
                        $compra = new Compra();
                        //update current code
                        $code->redeemed = true;
                        $code->redeemed_id = Str::random(10);
                    
                        // create new redeemed code in db
                        $redeemedCode = new redeemCode();
                        $redeemedCode->user_id = $user->sub;
                        $redeemedCode->code_id = $code->id;
                        $redeemedCode->redeemed_id = $code->redeemed_id;
    
                        //después insertar el deck comprado en la tabla compras para que le aparezca al usuario en el front
                        $compra->uid             = random_int(4,9);
                        $compra->nombre_deck     = $code->deck->nombre_deck;
                        $compra->precio_unitario = $code->deck->precio;
                        $compra->user_id         = $user->sub;
                        $compra->qty             = 1;
                        $compra->purchase_from   = "código de canje";
                        $compra->user_paypal_email  = "none";

                        //Guardar en BD
                        $code->save();
                        $redeemedCode->save();
                        $compra->save();

                        unset($code->redeemed_id);

                        $data = [
                            'code' => 200,
                            'statues' => 'success',
                            'message' => 'El código ha sido canjeado con éxito',
                            'data' => $code,
                            'request' => $params_array['codigo']
                        ];
                    }
                }
                else {
                    $data = [
                        'code' => 400,
                        'statues' => 'Not found',
                        'message' => 'El código introducido no es válido',
                        'data' => $code,
                        'request' => $params_array['codigo']
                    ];
                }
            }
        } 

        return response()->json($data, $data['code']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Recoger datos por post
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);
        
        if(!empty($params_array))
        {
            //Conseguir usuario identificado
                $jwtAuth = new JwtAuth();
                $token = $request->header('Authorization', null);
                $user = $jwtAuth->checkToken($token, true);

            //Validar los datos
                $validate = Validator::make($params_array, [
                    'title' => 'required',
                    'content' => 'required',
                    'category_id' => 'required',
                    'image'       => 'required'
                ]);

                if ($validate->fails()) {
                    $data = [
                        'code' => 400,
                        'status' => 'success',
                        'message' => 'No se ha guardado el post, faltan datos'
                    ];
                } else {
                    //Guardar el artículo
                    $post = new Post();
                    $post->user_id      = $user->sub;
                    $post->category_id  = $params->category_id;
                    $post->title        = $params->title;
                    $post->content      = $params->content;
                    $post->image        = $params->image;
                    $post->save();

                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'post' => $post
                    ];
                    
                }
                

        }
        else
        {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Envía los datos correctamente.'
            ];
        }


        //Devolver la respuesta
        return response()->json($data, $data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        if(is_object($post))
        {
            $data = [
                'code' => 200,
                'status' => 'success',
                'post' => $post
            ];
        }
        else
        {
            $data = [
                'code' => 404,
                'status' => 'error',
                'message' => 'La entrada no existe.'
            ];
        }

        return response()->json($data, $data['code']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Recoger los datos por post
            $json = $request->input('json', null);
            $params_array = json_decode($json, true);
            $data = array(
                'code' => 400,
                'status' => 'error',
                'post' => 'Datos enviados incorrectamente.'
            );
        //Conseguir usuario identificado
            $user = $this->getIdentity($request);


        if(!empty($params_array))
        {

            //Validar los datos
            $validate = Validator::make($params_array, [
                'title' => 'required',
                'content' => 'required',
                'category_id' => 'required'

            ]);
            
            if($validate->fails())
            {
                $data['errors'] = $validate->errors();
                return response()->json($data, $data['code']);
            }
            //Eliminar lo que no queremos actualizar
                unset($params_array['id']);
                unset($params_array['user_id']);
                unset($params_array['created_at']);
                unset($params_array['user']);

            //Buscar el registro que se va a actualizar
            $post = Post::where('id', $id)
                        ->where('user_id', $user->sub)
                        ->first();

            if(!empty($post) && is_object($post))
            {
                //Actualizar el registro 
                $post->update($params_array);

                //Devolver respuesta
                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'post' => $post,
                    'changes' => $params_array
                );
            }

            /*
                $where = [
                    'id' => $id,
                    'user_id' => $user->sub
                ];

                $post = Post::updateOrCreate($where, $params_array);
            */

        }

        return response()->json($data, $data['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        //Conseguir usuario identificado
        $user = $this->getIdentity($request);
        
        //Conseguir el registro
        $post = Post::where('id', $id)
                            ->where('user_id', $user->sub)
                            ->first();
        if(!empty($post))
        {    
            //Borrarlo
                $post->delete();

            //Devolver
            $data = [
                'code' => 200,
                'statis' => 'success',
                'post' => $post
            ];
        }
        else
        {
            $data = [
                'code' => 404,
                'status' =>'error',
                'message' => 'El post no existe.'
            ];
        }

        return response()->json($data, $data['code']);
    }

    private function getIdentity($request)
    {
        //Conseguir usuario identificado
        $jwtAuth = new JwtAuth();
        $token   = $request->header('Authorization', null);
        $user    = $jwtAuth->checkToken($token, true);

        return $user;
    }

    public function upload(Request $request)
    {
        //Recoger la imagen de la petición
        $image = $request->file('file0');

        //Validar la imagen
        $validate = Validator::make($request->all(), [
            'file0' => 'required|image|mimes:jpg,jpeg,png,gif'

        ]);

        //Guardar la imagen
            if(!$image || $validate->fails())
            {
                $data = [
                    'code' => 400,
                    'status' =>'error',
                    'message' => 'Error al subir la imagen.'
                ];
            }
            else
            {
                $image_name = time().$image->getClientOriginalName();

                Storage::disk('images')->put($image_name, File::get($image));
                
                $data = [
                    'code'      => 200,
                    'status'    =>'success',
                    'image'     =>  $image_name
                ];
            }

        //Devolver datos

        return response()->json($data, $data['code']);
    }

    public function getImage($filename)
    {
        //comprobar si existe el fichero
        $isset = Storage::disk('images')->exists($filename);

        if($isset)
        {
            //Conseguir la imagen
            $file = Storage::disk('images')->get($filename);
            
            //Devolver la imagen
            return new Response($file, 200);
        
        }
        else
        {
            //Mostrar error
            $data = [
                'code' => 404,
                'status' => 'error',
                'message' => 'La imagen no existe'
            ];
        }

        return response()->json($data, $data['code']);

    }

    public function getPostsByCategory($id)
    {
        $posts = Post::where('category_id', $id)->get();

        return response()->json([
            'status' => 'success',
            'posts' => $posts
        ], 200);
    }

    public function getPostsByUser($id)
    {
        $posts = Post::where('user_id', $id)->get();

        return response()->json([
            'status' => 'success',
            'posts' => $posts
        ], 200);
    }


    public function storePaypalInfo(Request $request) {
        $json        = $request->input('json', null);
        $paramsArray = json_decode($json, true);

        $data = [
            'data' => $json,
            'code' => 200,
            'mames' => "que pedo?"
        ];

        return response()->json($data, 200);
    }
}
