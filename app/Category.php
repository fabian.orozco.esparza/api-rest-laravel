<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    //Relación de 1:n
    public function posts(){
        return $this->hasMany('App\Post');
    }
}
