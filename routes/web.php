<?php

use App\Http\Controllers\MisComprasController;
use App\Http\Middleware\ApiAuthMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/pruebas/{nombre?}', function($nombre = null) {
    $texto = '<h2>Texto ruta</h2>';
    $texto .= $nombre;

    return view('pruebas', ['texto' => $texto]);

    
});

Route::get('/animales', 'pruebasController@index');

Route::get('/test-orm', 'pruebasController@testOrm');


//Rutas del controlador de usuario
Route::post('/api/register', 'UserController@register');
Route::post('/api/login', 'UserController@login');
Route::put('/api/user/update', 'UserController@update');
Route::put('/api/user/updatePassword', 'UserController@updatePassword');
Route::post('/api/user/upload', 'UserController@upload');
Route::post('api/recoverpassword', 'UserController@recoverPassword');
Route::get('/api/user/avatar/{fileName}', 'UserController@getImage');
Route::get('/api/user/detail/{id}', 'UserController@detail');
Route::get('api/verify/{token}', 'UserController@verifyUser');


//Rutas del controlador de categorías
Route::resource('/api/category', 'CategoryController');


//Rutas del controlador de entradas
Route::resource('/api/post', 'PostController');
Route::post('/api/post/upload', 'PostController@upload');
Route::get('/api/post/image/{fileName}', 'PostController@getImage');
Route::get('/api/post/category/{id}', 'PostController@getPostsByCategory');
Route::get('/api/post/user/{id}', 'PostController@getPostsByUser');

// Rutas para canjear el código
Route::post('/api/code/redeem', 'PostController@redeemCode');
//Rutas catálogos
Route::get('/api/codes', 'PostController@getCodes');

//Rutas cards
Route::post('/api/allcards', 'cardsController@getAllDecks');
Route::get('/api/allcardsNoLogin', 'cardsController@getAllDecksNoLogin');
Route::get('/api/card/getCard/{fileName}', 'cardsController@getCardImage'); //get card one by one
Route::get('/api/getInstitutionalEvents', 'cardsController@getInstitutionalEvents'); //Get image of the institutional events


//PayPal Payments
Route::post('/api/setNewPayment', 'payPalPaymentsController@storePaypalInfo');

//Rutas mis compras
Route::resource('api/mis-compras', 'MisComprasController');
Route::get('api/downloadZip/{deckName}', 'MisComprasController@downloadDeck');


