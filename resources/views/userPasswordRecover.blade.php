<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recuperación de contraseña</title>
</head>
<body>
<strong>Haz solicitado un cambio de contraseña desde la página de Imarain, porfavor da click en el siguiente enlace que te dirigirá a una página segura y cambia tu contraseña.</Strong>
    <p><a href="https://imarain.com/recover-password?idToken={{$token}}">Click aquí para cambiar tu contraseña</a></p>
    <p>si no funciona el botón de arriba copia y pega el siguiente enlace en la barra de direcciones de tu navegador</p>
    <p>https://imarain.com/recover-password?idToken={{$token}}</p>
    <p>Si has recibido este correo y no sabes de qué se trata por favor eliminalo de tu bandeja de entrada.</p>
</body>
</html>