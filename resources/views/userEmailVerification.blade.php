<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>verificación de cuenta</title>
</head>
<body>
<strong>Hola! Para completar tu registro en Imarain por favor verifica tu correo haciendo click en el enlace de abajo.</Strong>
    <p><a href="https://angular-blog-gusml.ondigitalocean.app/api/api/verify/{{$token}}">Verifica tu email aquí</a></p>
    <p>si no funciona el botón de arriba copia y pega el siguiente enlace en la barra de direcciones de tu navegador</p>
    <p>https://angular-blog-gusml.ondigitalocean.app/api/api/verify/{{$token}}</p>
    <p>Si has recibido este correo y no sabes de qué se trata por favor solo ignoralo y eliminalo de tu bandeja de entrada.</p>
</body>
</html>
