<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<strong>Hola! Se ha regitrado un nuevo usuario en la aplicación imarain decks a las {{ $registeredUser->created_at }}.</Strong>
    <p>Datos del usuario que ha realizado el registro:</p>
    <ul>
        <li>Nombre: {{ $registeredUser->name }}</li>
        <li>Apellido: {{ $registeredUser->surname }}</li>
        <li>Email: {{ $registeredUser->email }}</li>
    </ul>
</body>
</html>