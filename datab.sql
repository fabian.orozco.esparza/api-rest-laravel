CREATE DATABASE  api_rest_laravel;
USE api_rest_laravel;

-- Create a new table called 'TableName' in schema 'SchemaName'
-- Drop the table if it already exists
IF OBJECT_ID('SchemaName.TableName', 'U') IS NOT NULL
DROP TABLE SchemaName.TableName
GO
-- Create the table in the specified schema
CREATE TABLE api_rest_laravel.users
(
    id          INT(255) auto_increment not null, -- primary key column
    name            varchar(50) NOT NULL,
    surname         varchar(100),
    role            varchar(20),
    email           varchar(255) NOT NULL,
    password        varchar(255) NOT NULL,
    descripcion     text,
    image           varchar(255),
    created_at      datetime DEFAULT NULL,
    updated_at      datetime DEFAULT NULL,
    remember_token  varchar(255)
    
    CONSTRAINT pk_users PRIMARY KEY(id)
)ENGINE=InnoDb;
GO

CREATE TABLE categories(
    id          INT(255) auto_increment not null,
    name        varchar(100)
    created_at  datetime DEFAULT NULL,
    updated_at  datetime DEFAULT NULL,

    CONSTRAINT pk_categories PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE posts(
    id          INT(255) auto_increment not null,
    user_id     int(255) NOT NULL,
    category_id int(255) not null,
    title       varchar(255) not null,
    content     text not null,
    image       varchar(255),
    created_at  datetime DEFAULT NULL,
    updated_at  datetime DEFAULT NULL,

    CONSTRAINT pk_posts PRIMARY KEY(id)
    CONSTRAINT  fk_post_user FOREIGN KEY(user_id) REFERENCES users(id)
    CONSTRAINT fk_post_category FOREIGN KEY(category_id) REFERENCES categories(id)
    
)ENGINE=InnoDb;